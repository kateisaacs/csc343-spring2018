## Project Milestone 2

Version History: 

- Released 2018/2/16

The subject of the project is a stand alone ordering system for a restaurant,
such as those seen locally as places like Panera and Jimmy's Pita and Poke.

In this milestone we are:

- Stating the context of use of our stand alone ordering system
- Generating preliminary designs

The artifacts generated should be presented in a single HTML page as described
below. No style information should be in the HTML tags. Do not use
HTML tags like `<center>`, `<b>`, or `<i>` to alter visual appearance.
Programmatically added SVG elements may have style attributes.

Use the following link to create your github repository for this assignment:
[https://classroom.github.com/a/weWXyamW](https://classroom.github.com/a/weWXyamW)

Your git repository should contain one HTML file named `PM2.html` as well as
one CSS file. It should also include any images or media needed for the report
and `PM2.html`. 

If the repository link fails, please let me know AND email support@github.com.
Then, create a separate private repository on bitbucket.org (which offers free
private repositories) and add me to the repository (username: kateisaacs) and
I will clone from there at the deadline. I will need access to this repository
at least one hour before the deadline.

### Content

For the purposes of this assignment, we are assuming the user goal of ordering
lunch.

#### Context of Use

Write clearly the context of use for your stand alone ordering system.

For example, as we discussed in class, some expect people walking up to be
seated later, some are for To-Go users only, and some are table-side. Some
accept credit only and some allow cash. Any of these is fine, but this should
be made clear.

#### Interface Design Sketches -- Brainstorming

Include images of your rough brainstorming sketches. Number each design. There
should be at least ten. Then, group the designs by commonality and explain why
they were grouped the way there were but what the differences between them
are. You will be graded on the diversity of designs, their appropriateness to
the user's task, and the thoroughness and thoughtfulness in explaining their
grouping. While you will not be graded on artistic ability, please use
annotations as necessary to communicate your design to others. 

Having difficulty coming up with ten unique designs? Consider looking through
design patterns. Consider looking at existing websites and apps, which may or
may not have to do with food. Consider picking a (potentially unrelated)
object and theming a design around that.

#### Design Sketches -- Elaboration

Pick three of your most promising brainstorming designs and sketch an
elaborated design. The elaborated design should show the layout and
functionality. For each one, it should also explain the conceptual model,
metaphor, and/or inspiration as well as the advantages and disadvantages of
the design, including with respect to design principles/rationale, but also
with any other issues you think are of importance.

You will be graded on the choice of the three designs and whether they cover
your design space, the thoroughness in communicating the design and its
interactions, the thoroughness and appropriateness of the concept/metaphor
explanation as well as the explanation of the advantages and disadvantages. I
expect at least three advantages and disadvantages of each design.



### Format

Please write your report in the format described below. As with the previous
PM, the bulk of the credit is in the content, not in the format. 

In your CSS, all rules must follow the indention style:

```
selector {
    property: value;
    property: value;
    .
    .
    .
}

selector {
    property: value;
    property: value;
    .
    .
    .
}
```
Note the selector and its braces appear on separate lines from the properties. Also,
there must be a blank line between each rule.

Your CSS file must be no more than 65 lines long including blank spacing lines.

I will add the following to account for OSX differences. It does not count
against your 65 lines:
```
h2, h4, p {
    -webkit-margin-before: 0;
    -webkit-margin-after: 0;
}
``` 

The title should be "FamilyName, PreferredName - PM2" where FamilyName is your
family name and PreferredName is what you prefer to be called.

Your report should have static menu on the left that links to each of
the five content sections: Context of Use, Rough Designs, Design \#1, Design
\#2, and Design \#3. Each link should be preceded by a right pointing arrow. 

The page should have a black background, and gray (`#DDD`) text.

Links, headers, image borders, and arrows should be coral (`#CC6666`).

All images should be at most 500 pixels wide and have a 2 pixel solid border.
Clicking on an image should display the full sized image.

The main text font should be 12 point sans-serif. All paragraphs should be
justified. The main content should be 640 pixels wide. 

The navigation menu should be 150 pixels wide. The links in the menu should be
indented 10 pixels. There should be at least 10 pixels of spacing between each
link.

There should be a 25 pixel buffer between the menu and the content. There
should be at least 40 points of space between each section of the content.
Content, starting with Context of Use, should start at the top without a large
40 point space.

Section headers should be 20 points and sub-section headers should be 14 point
and bold. There should be 10 points of spacing between each section header and
its content. There should be 7 points of spacing between each sub-section
header and its content. There should be 14 points of spacing between each
subsection.

Advantages and Disadvantages should have the default list style. There should
be 12 points of spacing between each list element.

A movie demonstrating the report format is available [here:
PM2.mov](videos/PM2.mov)

Sample images:
![](images/PM2-figure1.png)
![](images/PM2-figure2.png)

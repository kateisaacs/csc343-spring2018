# Assignments for UA CSc 343 Spring 2018

This repository contains descriptions of homework assignments and related documents.

- Homework Assignment 1: [HA1.md](HA1.md)
- Homework Assignment 2: [HA2.md](HA2.md)
- Project Milestone 1: [PM1.md](PM1.md)
- Homework Assignment 3: [HA3.md](HA3.md)
- Project Milestone 2: [PM2.md](PM2.md)
- Project Milestone 3: [PM3.md](PM3.md)
- Project Milestone 4: [PM4.md](PM4.md)
- Homework Assignment 4: [HA4.md](HA4.md)
- Project Milestone 5: [PM5.md](PM5.md)
- Project Milestone 6: [PM6.md](PM6.md)
- Project Presentation: [Presentation.md](Presentation.md)
- Project Milestone 7: [PM7.md](PM7.md)

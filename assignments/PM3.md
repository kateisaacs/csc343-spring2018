## Project Milestone 3

Version History: 

- Revised 2018/3/18 - Make explicit that individual reports should also be in
  PM3 directory
- Revised 2018/3/12
- Released 2018/3/2

The subject of the project is a stand alone ordering system for a restaurant,
such as those seen locally as places like Panera and Jimmy's Pita and Poke.

**This assignment is due in two parts:**

A: Due Friday March 16, 8PM (group), 8:30 PM (individual)

B: Due Friday March 23, 8PM (group), 8:30 PM (individual)

The submission for the group part of B should include/extend the submission
for the group part of A. Both parts should have individual submissions.

If you submit parts of B at the part A deadline, I will give you early
feedback on them and allow you to re-submit them.


In this milestone we are:

- Coming to consensus with our newly formed group members
- Stating the agreed-upon context of use of our stand alone ordering system
- Stating clearly what it means to place an order for lunch and prioritizing
  additional features
- Creating and demonstrating a paper prototype
- Explaining the rationale for your choice of design concept
- Individually stating our contributions and the contributions of our team
  members to this milestone.

The artifacts generated should be presented in a single HTML page as described
below. No style information should be in the HTML tags. Do not use
HTML tags like `<center>`, `<b>`, or `<i>` to alter visual appearance.
Programmatically added SVG elements may have style attributes.

I have created a bitbucket repository for each team. Create a PM3 folder in
your team's repository. It should contain one HTML file named `PM3.html.` It
should also include any CSS files, Javascript files, images or media needed
for the report and `PM3.html`. The group portions of this project are due
2018/3/16 and 2018/3/23 respectively at 8PM. 

I have created a bitbucket repository for each individual. Create a PM3 folder
in your individual repository. It should contain a text file named
`PM3-A-evaluations.txt` and later `PM3-B-evaluations.txt.` The file should
have the content described under Individual Evaluations below. This file is
due 2018/3/16 and 2018/3/23 respectively at 8:30PM (half an hour after the
rest of the project).


### Content - Header information Due in Part A

At the top of your report, state the names of all the team members as well as
your team name. You can name your team what you like as long as it is
appropriate for school. 

#### Context of Use - Due in Part A

Write clearly the context of use for your stand alone ordering system as in
PM2.

For example, as we discussed in class, some expect people walking up to be
seated later, some are for To-Go users only, and some are table-side. Some
accept credit only and some allow cash. Any of these is fine, but this should
be made clear.

In PM2, some of you discussed the type of restaurant you would support. Your
context of use should refine this in terms of what your expectations are
regarding the menu and what the user could do. It may help to be specific.
Instead of a general system for your type of restaurant, come up with a
concrete idea of what your restaurant is, what kind of food they serve, and
what's on their menu. You may want to constrain the menu at first but discuss
how it could become more complicated as the project allows. 

Write your context of use in prose (i.e., paragraphs) under a header "Context
of Use." 

#### Statement of Tasks - Due in Part B

The header for this section should be "Statement of Tasks."

For this project, we are focusing on the main goal of a user ordering lunch.
As a group, construct a set of sub-tasks that are the bare minimum for
supporting this goal. Write this as a (potentially nested) ordered list under
a sub-header "Minimum Proposed Tasks." Underneath the list, write a paragraph
explaining your rationale for these minimum tasks. You may refer to PM1 and
other experience to support this rationale. 

The purpose of the Minimum Tasks is to initially constrain the focus of the
project. In that way, it acts like a project proposal. 

Create an ordered list of sub-tasks that are non-essential but you foresee
increasing the usability of your system. The list should be ordered by
importance. Write this as a (potentially nested) ordered list under a
sub-header "Extended Proposed Tasks." Underneath the list, write a paragraph
explaining your rationale for these tasks. You may refer to PM1 and other
experience to support this rationale.

The purpose of the Extended Tasks is to:

1. Anticipate potential issues that come up during evaluation.
2. Aid in thinking about the extensibility of your design.
3. Aid in planning should the design, implementation, and evaluation of your
   Minimum Tasks run smoothly enough to allow you to extend the project.

The main idea is to start small and focused so you can have a complete
interface prototype but allow room to grow should the initial concept be too
small. You may want to start with the assumption of a limited menu for the
minimum tasks, but propose how the menu could become more complicated and thus
how the tasks should grow to support that. 


#### Prototype Design - Due in Part B

From your combined PM1 and PM2 submissions, as well as your stated context and
tasks above, select and elaborate on an group design. 

Include the design as an image. Beneath the image, write your explanation of
the design concept as a paragraph. 

Beneath the design concept paragraph, create a sub-header "Design Rationale."
Write your rationale for the design. Given the amount of background you now
have from the previous milestones, I expect this rationale to be thorough.

Beneath the "Design Rationale", create a sub-header called "Supporting
Artifacts". Each artifact (or set of artifacts if there are many
sketches/images of the same idea) should be in its own collapsible area,
following the accordion interaction design pattern. Do not include all of your
PM1s and PM2s, but you may include excerpts from each as referenced by your
design rationale. 

#### Paper Prototype - Due in Part B

Create a paper prototype. Embed a movie showing the prototype through three
“ordering lunch” tasks where in each one, the demonstration is different. For
example, one can show the scenario of a user who is familiar with the
interface and knows exactly what she wants while another could show a user who
has to do more searching.

Remember that this paper prototype will be used for evaluation by people
outside this class in PM4. It should be nice enough that those participants
will understand what is going on during evaluation. 

The paper prototype should be under the header "Paper Prototype."


### Format - Due in Part A and Part B

The only constraints on the format, other than what is described above are:

- The report should be reasonably readable. For example, black text on a black
  background is not reasonable. 
- The headers should be differentiable from the body text and spacing should
  be used to help readability.
- There should be some persistent way for me to navigate between sections.
- The title should have your group name.  
- The report content should be 640 pixels wide with a 12 point sans-serif
  font for the report text.


### Individual Evaluations - Required for both Part A and Part B

This content should be written by each individual after the project is
submitted. Please place in a folder named `PM3` in your individual repository.

Since this was added after PM3-A was due, this is not a requirement for
PM3-A. For PM3-B, please call the file `PM3-B` (with whatever necessary
extention, I recommend `.txt` or `.md` or `.html`) and move the PM3-A file
into the folder `PM3` and name it `PM3-A` (with the matching extension).

The first paragraph should describe what your contributions to the project
milestone were:

```
Individual Contributions -- FamilyName, GivenName

My contributions were...
```

Then, for each of your team members, write a section headed with their name
explaining what their contributions were and rating them on:

1. Quality of Work
2. Timeliness of Work Completion
3. Contributions to Group Discussion
4. Cooperative and Supportive Attitude

Ratings should be one of [Below Expectations, Good, Above and Beyond]. 

Beneath the ratings you may provide more detail explaining your ratings or any
other information I should know.


Example:
```
Peer Assessment -- FamilyName, GivenName

GivenName's contributions were... 

1. Quality of Work: Good

2. Timeliness of Work Completion: Below Expectations

3. Contributions to Group Discussion: Good

4. Cooperative and Supportive Attitude: Good

GivenName did a good job on the paper prototype but was two days later than we
planned so we had to scramble to make the movie.
```

### Distribution

All team members should participate in the discussion of the context of use,
tasks, and initial design.

The team should strive to apportion equitably. It is up to the team how this
can be done. 

It may help to appoint different people to draft each writing section and then
someone else to check/edit the writing for completeness. Similarly, one person
can be in charge of assembling the webpage and another in charge of checking
it for compliance. 

One person may be in charge of designing the three initial scenarios, perhaps
with storyboards or step-by-step procedures, while another is in charge of
crafting the paper prototype (but others may help). A third person can then
prepare those storyboard for the accordion section of the report. One to two
people can demonstrate the paper prototype while a third one films. 

## Project Milestone 4

Version History: 

- Revision 2018/3/20 - Fix off by one error in Release date.
- Released 2018/3/20

The team portion of this milestone is due Friday March 30, 8 PM.

The peer reviews for this milestone are due Friday March 30, 8:30 PM.

In this milestone we are:

- Design an observational evaluation for your paper prototype
- Conduct the observational evaluation you designed.
- Analyzing the results of the evaluation 
- Reporting on the results of your analysis
- Individually stating your contributions and the contributions of your team
  members to this milestone.

The artifacts generated should be presented in a single HTML page as described
below. No style information should be in the HTML tags. Do not use
HTML tags like `<center>`, `<b>`, or `<i>` to alter visual appearance.
Programmatically added SVG elements may have style attributes.

Create a PM4 folder in your team's repository. It should contain one HTML file
named `PM4.html.` It should also include any CSS files, Javascript files,
images or media needed for the report and `PM4.html`. 

Create a PM4 folder in your individual repository. It should contain a text
file named `PM4-evaluations.txt.` The file should have the content described
under Individual Evaluations below. This is the same format used in PM3. 

### Conducting the Evaluations

You should conduct the evaluation on one person for each team member.
Therefore, if your group has 3 people, you will conduct 3 evaluations. If your
group has 4 people, you will conduct 4 evaluations. Not every team member must
be present at every evaluation but I expect each team member will participate
in some form (e.g., facilitator, observer) in at least one evaluation.

The materials related to your evaluation should be included as below:


### Content - Header Information

At the top of your report, state the names of all the team members as well as
your team name. Also include an image of your paper prototype design. 

#### Section I: Observation Design

Create a section that includes your briefing and de-briefing scripts, a
description of your tasks (what you will ask them to do), and a list of your
interview questions for the observation. Each of these four artifacts should
be clearly separated from each other so I can tell them apart. 

Focus on the task of ordering lunch for first-time users. 

You will be graded on the appropriateness of these materials. You may include
a Rationale sub-section to explain any choices you made.

#### Section III: Observational Analysis

After conducting the observations, analyze the results of the observations and
report on the analysis as described in Lectures 14 and 15.


#### Section III: Suggestions for Improvement

Based on the analysis of the previous section, create a prioritized list of
suggestions for improvements upon the design. Included in these suggestions
may be design elements that should not change. Explain the prioritization.

#### Appendix: Observation Artifacts

Your observation artifacts should be included in the report. Artifacts can
include raw notes, audio, visual, etc. If you did record audio or visual but
your participants would prefer these not be shared, a transcript should be
included. You will be graded on the appropriateness of these artifacts in
terms of level of detail and clear separation of facts from inference.


### Format 

The only constraints on the format, other than those described above, as are
in PM3:

- The report should be reasonably readable. For example, black text on a black
  background is not reasonable. 
- The headers should be differentiable from the body text and spacing should
  be used to help readability.
- There should be some persistent way for me to navigate between sections.
- The title should have your group name.  
- The report content should be 640 pixels wide with a 12 point sans-serif
  font for the report text.


### Individual Evaluations 

This content should be written by each individual after the project is
submitted. Please place in a folder named `PM4` in your individual repository.
Please name the file `PM4` (with whatever necessary extention, I recommend
`.txt` or `.md` or `.html`).


The first paragraph should describe what your contributions to the project
milestone were:

```
Individual Contributions -- FamilyName, GivenName

My contributions were...
```

Then, for each of your team members, write a section headed with their name
explaining what their contributions were and rating them on:

1. Quality of Work
2. Timeliness of Work Completion
3. Contributions to Group Discussion
4. Cooperative and Supportive Attitude

Ratings should be one of [Below Expectations, Good, Above and Beyond]. 

Beneath the ratings you may provide more detail explaining your ratings or any
other information I should know.


Example:
```
Peer Assessment -- FamilyName, GivenName

GivenName's contributions were... 

1. Quality of Work: Good

2. Timeliness of Work Completion: Below Expectations

3. Contributions to Group Discussion: Good

4. Cooperative and Supportive Attitude: Good

GivenName did a good job on the paper prototype but was two days later than we
planned so we had to scramble to make the movie.
```

### Distribution

All team members should participate in the discussion of evaluation design,
observations themselves, and suggestions for improvement. At least two team
members should participate in the analysis.

The team should strive to apportion equitably. It is up to the team how this
can be done. 

It may help to appoint different people to draft each writing section and then
someone else to check/edit the writing for completeness. Similarly, one person
can be in charge of assembling the webpage and another in charge of checking
it for compliance. 

One way to divide up the observations is to have each team member recruit one
participant and then act as facilitator for that participant and then observer
on someone else's participant. However, if this proves difficult for a team
member, the work can be divided up differently with the team member
participating less in the observations doing more work on the analysis,
writing, and/or web page assembly.


## Homework Assignment 3

Version History: 

- Edit 2018/2/9: Add more explanation to Draw By Bounding Box
- Edit 2018/2/8: Changed title from HA2 to HA3
- 2018/2/8: Released

In this assignment, we are:

- improving an interface to adhere to design principles
- getting more practice GUI programming in HTML/CSS/Javascript
- getting more practice with the MVC design pattern

This assignment is based on HA2 which in turn was based on one from CSC 210 in
Fall 2017, which was also about GUI programming (in Java) and design patterns.
Neither HA2 or the CSC 2010 assignment were about UI design.

In this assignment, we iterate on the UI from HA2 using suggestions from class
in accordance with design principles. You may start from your HA2 submission
or you may start from this sample solution: 
[https://github.com/UACSc343Spring2018/HA3-base.git](https://github.com/UACSc343Spring2018/HA3-base.git)

You will also include a README.md file with this assignment. In the README.md,
you will describe your rationale for the design choices you made as indicated
below. Your rationale should refer to design principles. 

You will be graded on your design choices with their rationale, functionality,
adherence to MVC, and adherence to any mandates in this document. The design
choices/rationale here only apply to the parts of the submission you must
document in the README.md. 

Do not use any Javascript libraries. The HTML, Javascript, and CSS should be
in separate files. No style information should be in the HTML tags. Do not use
HTML tags like `<center>`, `<b>`, or `<i>` to alter visual appearance.
Programmatically added SVG elements may have style attributes.

Use the following link to create your github repository for this assignment:
[https://classroom.github.com/a/nvwRXvcm](https://classroom.github.com/a/nvwRXvcm)
Your git repository should contain one HTML file named `HA3.html` as well as a
CSS file, a JS file, and a README.md file. 

The webpage should consist of a set of controls, a set of instructions, and an
SVG drawing area.  The title of the webpage should be "Last Name, First Name -
HA3" where your last name and first name are used.

### Persistent Elements

#### Controls

The controls consist of nine buttons and two text fields. 

Three buttons are labeled Rectangle, Triangle, Ellipse.

Three buttons are labeled Red, Green, Blue.

The text fields have external labels, Height and Width.

Two buttons are labeled Undo and Redo.

The final button is labeled Clear.

Document in the README.md your rationale for the layout and style of these
controls.

#### SVG

The SVG element should be 600 pixels square and have a 1 pixel solid black
border.

#### Documentation

Include documentation on the webpage about how to operate the program.
Document in the README.md your rationale for the layout, style, and content of
this documentation.


### Functionality

The purpose of this webpage is to let users draw shapes in the SVG.  The type
and color of the shape is determined by the last clicked shape (Rectangle,
Triangle, Ellipse) and color (Red, Green, Blue) button pressed. The system
should show which shape and color is currently selected by altering the look
of the button. In the README.md file, describe your rationale for choosing the
style you did. Note: It is recommended you add `-webkit-appearance: none` if
you choose to do style changes as this code will be tested on macos.

If a value in the height or width field is bad (e.g., not a number, 0 or
less), the user should be alerted of the error and barred from drawing a shape
by clicking until the error is fixed. In the README.md file, describe your
rationale for choosing your policy for this error handling.

Use an SVG path element or SVG polygon element to draw the triangle. It should
be an isosceles triangle with the free edge at the bottom.

Clicking on the Undo button should remove the last drawn shape that is still
being drawn. Clicking on the Redo button should should undo the last Undo.
These buttons should behave appropriately in corner cases. In the README.md
file, describe the policies you chose for your Undo/Redo buttons and your
rationale for those policies in terms of design principles.

Clicking on the Clear button should prompt the user for confirmation. If
confirmation is given, it should remove all shapes from the SVG. In the
README.md file, describe the rationale behind the design of your confirmation
message.

#### Draw by Clicking

When the user clicks inside the SVG bounds, a shape should be drawn centered
around the mouse pointer. Depending on where the user clicks, the entire shape
may not be visible if it extends past the SVG bounds.

The bounding box of the shape is determined by the values in Height and Width
text fields. 

When the user hovers inside the SVG bounds, a pre-image of the shape that
would be drawn on click should be shown. 

#### Draw by Bounding Box (Dragging)

Alternatively, the user can communicate the size of the shape by
drawing/dragging from one corner of the shape's bounding box to another. The
system should recognize it is in this drawing mode when the user starts the
action with a shift-click.  Note the width/height fields should reflect the
state of the bounding box.

The outline of this drawing style is:

1. The user presses the mouse button at point (x1, y1)
2. The user drags the mouse while still holding down the mouse button.
3. The user releases the mouse button at point (x2, y2)
4. The largest shape that can fit in the rectangle implied by the corner
   points (x1, y1) and (x2, y2) is drawn.

The README.md should include your rationale for any design decisions made
regarding this feature.

#### Defaults

When the page loads, there should be no shapes in the SVG. The initial values
for width and height should be 100 each. Before any other button is clicked,
the UI should assume a red rectangle.


### Organization

Your Javascript code should demonstrate the Model-View-Controller pattern. 

The application data should be kept by the Model which notifies any Views upon
change.

Any changes to the DOM should be managed by the View. The SVG view should
re-draw based on the model rather than start with pre-loaded shapes. 

The Controller should act as the mediator, making changes to the model as
notified by the View.

If you choose to use an alternative formulation of MVC, it should be
documented, with references and explanation, in the README.md.

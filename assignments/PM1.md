## Project Milestone 1

Version History: 

- Released 2018/2/2

The subject of the project is a stand alone ordering system for a restaurant,
such as those seen locally as places like Panera and Jimmy's Pita and Poke.

In this milestone we are:

- Gathering data to support the creating of initial project requirements
- Performing User and Use analysis for the project 
- Stating initial requirements

The artifacts generated should be presented in a single HTML page as described
below. The repository includes the CSS for the HTML page. Do not alter the
CSS. No style information should be in the HTML tags. Do not use
HTML tags like `<center>`, `<b>`, or `<i>` to alter visual appearance.
Programmatically added SVG elements may have style attributes.

Use the following link to create your github repository for this assignment:
[https://classroom.github.com/a/V12RtZmj](https://classroom.github.com/a/V12RtZmj)
Your git repository should contain one HTML file named `PM1.html` as well as
the unaltered CSS and JS files. It should also include any images or media
needed for the report and `PM1.html`.


### Format

Please write your report in a tabbed webpage with sections for Interviews,
Observations, Scenarios, Task Analysis, and Requirements. These are color
coded in green, blue, red, light gray, and gray. The tabs are rounded on top.
The content should be 800px wide.  The shown body is rounded in all free
areas. The tab text should be 14 pt sans-serif. All body text should be 12 pt
san-serif unless otherwise stated.

Any images included should be centered and 300 pixels wide. There should be a
caption underneath. Both should be encased in a black rounded border. On
click, the full sized image should be shown.

![](images/PM1-figure.png)

All links should be in black (both visited and non-visited) and underlined.

Only the content in the active tab should be shown at any given time.

A movie demonstrating the report format is available [here:
PM1.mov](videos/PM1.mov)


### Data Gathering

#### Interviews

Conduct a short interview with two people not in this class regarding their
use and interest in ordering kiosks. Document the type of interview you chose
and what preparation (e.g., questions) you developed prior to the interview.
This description should be at the top of the tab content.

Document each interview by describing the person interviewed and the relevant
answers you received from the interviewee. Each interview report should be
surrounded by a green dashed & rounded border. An identifier for the person
(e.g., "Participant 1" or their partially anonymized name) should be at the
top in bold.

Interview content will be graded based on correctness in describing the type
of interview, preparation, appropriateness of questions, and completeness of
data gathered: No Answer, Minimally Detailed, Detailed, Thoroughly Detailed.

#### Observation

Conduct two observations of people using these kiosks. Do not observe people
without permission. Do not do anything that will make things difficult for the
restaurant or get you in trouble. Do not get in the way of paying customers.
You may observe an online video rather than in-person. However, do not use
marketing videos of kiosk systems, they should be real people using them in
real settings. If a video is used, the link to that video must be included. 

Observation data should record the who, what, and where questions discussed in
class. They should clearly separate facts from observations. Pay attention to
how quickly the user is able to perform the actions and how many mistakes they
make. Where do they seem to have difficulty? Describe the interface in
question. Using figures may help.

Each observation report should be surrounded by a blue dashed & rounded
border. Each observation report should have a title. If the observation was
done off of a video, a link to the video should be provided. Observation
content will be graded on correcteness, appropriateness, and completeness of
data gathered: No Answer, Minimally Detailed, Detailed, Thoroughly Detailed.

### Analysis

#### Personas & Scenarios

Develop one abstract user, describing the general population served by the
system. Then develop and describe two concrete personas with the main scenario
of using the system for ordering a meal.

The abstract user and main scenario should be at the top of the tab content as
in the image below. THe personas should be surrounded by a red dashed &
rounded border with a name for the persona at the top in bold. Personas will
be graded on appropriateness and thoroughness. The two personas should be
substantially different from one another. 

#### Task Analysis -- Ordering

Perform hierarchical task analysis on the task of using the system for
ordering a meal. 

Describe the objects, lists, and flags in the domain of your task analysis.
Each list should be titled in bold and surrounded by a black dashed & rounded
border.

Write all of the tasks and subtasks as a list. In the sample, the task numbers
are specified by hand, not with the `ol` functionality. For each sub-task
(including non-leaf sub-tasks), include a plan. The plans should be below the
list for each goal. Each node in the hierarchy should be surrounded by a solid
black rounded border. Each plan within a node should be surrounded by a dashed
blue rounded border and have blue monospace text. The title of each node
(except the top one) should be a link to the plan. 

Actions in the list which are expanded upon as nodes should be a link to that
node in the HTML page. Actions which do not have nodes (e.g., leaf actions)
should be italic. The color should explain why you chose not to decompose the
node--red means it is an atomic user action, blue means it is an atomic system
action.

The content of this will be graded on appropriateness of the actions and
decomposition, appropriateness of labeling any action atomic, and
thoroughness.

![](images/PM1-ta.png)


### Requirements

Create two lists, one for functional requirements and one for non-functional
requirements. Each list should have at least six items. Order each list by
importance. In text below the lists, explain the rationale for your ordering.

Both requirements lists should be numbered, with the type of the requirement
bolded at the top. The rationale should be listed below with the word
"Rationale" bolded. The two lists should be separated by solid black rounded
borders.

The content will be graded on the appropriateness of the requirements, the
correctness of the categorization of requirements, and the appropriateness and
thoroughness of the rationale for the ordering.

![](images/PM1-reqs.png)

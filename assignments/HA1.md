## Homework Assignment 1

Create a simple webpage using only HTML and CSS. Do not use any Javascript or
Javascript libraries. Style/presentation should be in a separate CSS file. No
style information should be in the HTML tags. Do not use HTML tags like
`<center>`, `<b>`, or `<i>` to alter visual appearance.

Use the following link to create your github repository for this assignment: 
[https://classroom.github.com/a/2r5WPT2i](https://classroom.github.com/a/2r5WPT2i)
Your git repository should contain one HTML file named `HA1.html` as well as a
CSS file and an image file.

The webpage should consist of two side-by-side columns. All elements should
have a minimum of five pixels of spacing between each other and the edges of
the browser. Use a twelve point sans serif font. The title of the webpage
should be "Last Name, First Name - HA1" where your last name and first name
are used.

#### Column 1

The first column should be 600 pixels wide. In it should be a single box with
a one pixel black border. Within the box there should be:

- Top, centered: An image of something that is not designed well (e.g.,
  difficult, frustrating, or clumsy to use), showing why you believe it is not
designed well. The "something" can be software but does not have to be. The
image should appear no more than 450 pixels wide on the page. Clicking on the
image should navigate to the full image.

- Underneath the image, left justified: Text explaining what the image is
  showing, why you chose it, and why it isn't designed well.

#### Column 2

The second column should fill the width of the rest of the browsing window. It
should have each label below in bold with your answer in a normal weight font:

- Preferred Name
- Github Profile
- Reference Webpages

The Github Profile response should be a clickable link to your Github profile.
Any webpage you looked at to help you complete this assignment (e.g., a
specific StackOverflow answer) should be listed under "Referenced Webpages".
The list should be indented 10 pixels as in the sample below:

![](images/HA1.png)

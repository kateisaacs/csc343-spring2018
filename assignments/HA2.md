## Homework Assignment 2

Version History: 

- Edit 2018/1/20: Clarify out-of-bounds drawing per Piazza. Clarify type of
  triangle. Fix typos.

In this assignment, we are exercising:

- GUI programming in HTML/CSS/Javascript
- the MVC design pattern

This assignment is based on one from CSC 210 in Fall 2017, which was also
about GUI programming (in Java) and design patterns. 

Neither assignment is about UI design. For this assignment, follow the UI
design described below. We will consider better designs later this semester.

Do not use any Javascript libraries. The HTML, Javascript, and CSS should be
in separate files. No style information should be in the HTML tags. Do not use
HTML tags like `<center>`, `<b>`, or `<i>` to alter visual appearance.
Programmatically added SVG elements may have style attributes.

Use the following link to create your github repository for this assignment:
[https://classroom.github.com/a/05OyHU_1](https://classroom.github.com/a/05OyHU_1) 
Your git repository should contain one HTML file named `HA2.html` as well as a
CSS file and a JS file.

The webpage should consist of a set of controls and an SVG drawing area.  The
title of the webpage should be "Last Name, First Name - HA2" where your last
name and first name are used.

![](images/HA2.png)

### Persistent Elements


#### Controls

The controls consist of seven buttons and two text fields. All controls should
be 120 pixels wide and match the order shown in the sample image above. 

The first three buttons are labeled Rectangle, Triangle, Ellipse.

The second three buttons are labeled Red, Green, Blue.

The text fields have external labels, Height and Width.

The final button is labeled Clear.

#### SVG

The SVG element should be 600 pixels square and have a 1 pixel solid black
border.


### Functionality

When the user clicks inside the SVG bounds, a shape should be drawn centered
around the mouse pointer. Depending on where the user clicks, the entire shape
may not be visible if it extends past the SVG bounds.

The bounding box of the shape is determined by the values in Height and Width
text fields. If bad values are given in those fields, a default of 0 should be
assumed.

The type and color of the shape is determined by the last clicked shape
(Rectangle, Triangle, Ellipse) and color (Red, Green, Blue) button pressed.
Use an SVG path element or SVG polygon element to draw the triangle. It should
be an isosceles triangle with the free edge at the bottom.

Clicking on the Clear button should remove all shapes from the SVG.

A movie demonstrating this functionality is available [here:
HA2.mov](videos/HA2.mov)

#### Defaults

When the page loads, there should be no shapes in the SVG. The initial values
for width and height should be 100 each. Before any other button is clicked,
the UI should assume a red rectangle.


### Organization

Your Javascript code should demonstrate the Model-View-Controller pattern. 

The application data should be kept by the Model which notifies any Views upon
change.

Any changes to the DOM should be managed by the View. 

The Controller should act as the mediator, making changes to the model as
notified by the View.

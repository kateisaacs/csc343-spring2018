# Assignments & Lectures for UA CSc 343 Spring 2018

This repository contains assignments and lectures


## Assignments - [assignments/README.md](assignments)
- Homework Assignment 1: [assignments/HA1.md](assignments/HA1.md)
- Homework Assignment 2: [assignments/HA2.md](assignments/HA2.md)
- Project Milestone 1: [assignments/PM1.md](assignments/PM1.md)
- Homework Assignment 3: [assignments/HA3.md](assignments/HA3.md)
- Project Milestone 2: [assignments/PM2.md](assignments/PM2.md)
- Project Milestone 3: [assignments/PM3.md](assignments/PM3.md)
- Project Milestone 4: [assignments/PM4.md](assignments/PM4.md)
- Homework Assignment 4: [assignments/HA4.md](assignments/HA4.md)
- Project Milestone 5: [assignments/PM5.md](assignments/PM5.md)
- Project Milestone 6: [assignments/PM6.md](assignments/PM6.md)
- Project Presentation: [assignments/Presentation.md](assignments/Presentation.md)
- Project Milestone 7: [assignments/PM7.md](assignments/PM7.md)

## Live-Coding Lectures - [lectures/README.md](lectures)



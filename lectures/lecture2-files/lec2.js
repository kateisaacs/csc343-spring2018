document.addEventListener("DOMContentLoaded", function(event) {
    var div1 = document.getElementById('div1');

    var counter = 0;

    var btn = document.createElement('input');
    btn.setAttribute('type', 'button');
    btn.setAttribute('value', counter);
    div1.appendChild(btn);

    btn.addEventListener('click', function(event) {
	counter += 1;
	btn.setAttribute('value', counter);
    });

    btn.addEventListener('mouseover', function(event) {
	btn.setAttribute('class', 'hover');
    });
    btn.addEventListener('mouseleave', function(event) {
	btn.setAttribute('class', '');
    });
});

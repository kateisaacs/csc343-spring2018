# Lecture 1 - HTML & CSS

In this class, we will be using HTML, CSS, and Javascript for our interfaces.
This lecture covers the basics of creating a webpage with HTML and CSS as well
as how to think of your webpage as it relates to the DOM.

HTML describes **content**. We use HTML to tell the web browser about the
content.

CSS describes **presentation**. We use CSS to tell the web browser how the
content should look.

## HTML

### HTML Elements

The basic unit of HTML is an **element** which is denoted by opening and
closing **tags**:

`<tag></tag>`

Tags may enclose content:

`<tag>I'm the content!</tag>`

Tags with no-conten can be self-closing (written with a single set of `<` and
`>`:

`<tag />`

Tags may also contain **attributes** which in turn have **values**.

`<tag attribute="value">I'm the content!</tag>`

Elements may be nested, as we will see below.

### Anatomy of an HTML document

We are using HTML5. We denote this using `<!DOCTYPE html>`. Then we define the
`<html>` element that contains a `<head>` and a `<body>` element. The `<head>`
element is assumed to contain metadata about the document, such as the
character set, the title of the document, and links to scripts and CSS. The
`<body>` element contains all of the content.

```
<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8" />
    <link rel="stylesheet" href="style.css" />
    <title>Skeleton Webpage</title>.
  </head>
  <body>
    <p>Hello, World!</p>
  </body>
</html>
```

Here I've used a `<p>` tag to denote `Hello, World!` as a text paragraph.

#### Note: an HTML document is organized as a tree.

The `<html>` element is the root of a tree. It has two children `<head>` and
`<body>` which in turn have children. Unlike node-link tree representations,
here parent-child relationships are shown by containment. 


### Common tags 

#### Describing specific content

`<p>` denotes a text paragraph, as seen above.

`<img>` denotes an image file. It has several attributes. The attribute `src`
denotes the location and name of the image file. The attribute `alt` is the
alternative text that can show when the image does not load (or the webpage is
viewed in a non-graphical browser). It should describe the missing content:

`<img src="cat.jpg" alt="A cat." />`

`<a>` denotes a link. The attribute `href` is where the link points. The
content of the `<a>` tag is the text that should be displayed.

`<a href="https://developer.mozilla.org/en-US/docs/Web/HTML">MDN HTML
Docs</a>`

#### Content containers

The tags `<div>` and `<span>` on their own do not cause the browser to render
a page in a specific way. However, they allow grouping of content to be styled
by CSS.

`<div>` denotes a block of content.

`<span>` denotes an in-line grouping of content.


### Common global attributes

You will be using `class` and `id` extensively in this course. These will be
used to find elements or sets of elements programmatically or define their
style in CSS.

Each tag instance may have multiple classes, separated in the class attribute
by spaces, e.g., `class="class1 class2 class3"`. Similarly, multiple tag
instances may share the same class.

Each tag instance may have at most one `id`, e.g., `id="MyUniqueID"`. The `id`
must be unique.

## CSS

Recall: CSS describes how the content should look. In this class, we contain
our CSS in a separate file and use the `<link>` tag in `<head>` to inform the
web browser. However, CSS can also be included within `<style>` tags or
attributes directly. Some examples you find online may specify their CSS in
these ways.

CSS describes **rules** for how elements should be styled. Each rule has a
**selector** to describe which elements it applies to and then a set of
**property: value** pairs to describe the style itself.

```
selector {
  property1: value1;
  property2: value2;
}
```

Let's use the properties `border-width` and `border-color` on all `<div>`
elements:

```
div {
  border-width: 1px;
  border-color: black;
}
```

We can be more specific though. Instead of choosing all divs, we can choose
ones of a specific class with a `.`:

```
.myclass {
  border-width: 1px;
  border-color: black;
}
```

We can be even more specific and choose a particular div element, using the
`id` attribute and `#`:

```
#mydiv {
  border-width: 1px;
  border-color: black;
}
```

### Rule Conflicts

When multiple rules apply to the same element, the *most specific* rule wins.
This can get complicated. Using your browser's developer tools can help.

## Browser Developer Tools

In Google Chrome, these are known as Chrome DevTools. Other browsers offer
similar services.

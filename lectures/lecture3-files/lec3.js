var lec3 = {
    svgNS: 'http://www.w3.org/2000/svg',
    signal: {
	increment: 'INCREMENT'
    }
};

var makeSignaller = function() {
    var _subscribers = [];

    return {
	add: function(s) {
	    _subscribers.push(s);
	},

	notify: function(args) {
	    for (var i = 0; i < _subscribers.length; i++) {
		_subscribers[i](args);
	    }
	}
    };
}

var makeModel = function() {
    var _count = 0;
    var _observers = makeSignaller();

    return {
	increment: function() {
	    _count += 1;
	    _observers.notify();
	},

	get: function() {
	    return _count;
	},

	register: function(fxn) {
	    _observers.add(fxn);
	}
    };
}

var makeSVGView = function(model, svgID) {
    var _svg = document.getElementById(svgID);

    var _cleanSVG = function() {
	while (_svg.firstChild) {
	    _svg.removeChild(_svg.firstChild);
	}
    }

    var _makeCircle = function(x, y) {
	var circle = document.createElementNS(lec3.svgNS, 'circle');

	circle.setAttributeNS(null, 'cx', x);
	circle.setAttributeNS(null, 'cy', y);
	circle.setAttributeNS(null, 'r', 15);
	circle.setAttributeNS(null, 'fill', 'blue');

	_svg.appendChild(circle);
    };

    return {
	render: function() {
	    _cleanSVG();
	    var count = model.get();

	    for (var i = 0; i < count; i++) {
		var y = Math.floor(i/30);
		var x = i - y * 30;
		_makeCircle(x * 30 + 15, y * 30 + 15);
	    }
	}
    };
}

var makeButtonView = function(model, btnID) {
    var _btn = document.getElementById(btnID);
    var _observers = makeSignaller();

    var _fireIncrementEvent = function() {
	_observers.notify({
	    type: lec3.signal.increment
	})
    };

    _btn.setAttribute('value', model.get());
    _btn.addEventListener('click', _fireIncrementEvent);

    return {
	render: function() {
	    _btn.setAttribute('value', model.get());
	},

	register: function(fxn) {
	    _observers.add(fxn);
	}

    };
}

var makeController = function(model) {
    var _increment = function () {
	model.increment();
    }

    return {
	dispatch: function(evt) {
	    switch(evt.type) {
		case lec3.signal.increment:
		    _increment();
		    break;
		default:
		    console.log('Unknown Event Type: ', evt);
	    }
	}
    };
}

document.addEventListener("DOMContentLoaded", function(event) {
    var model = makeModel();
    var btnView = makeButtonView(model,'button1');
    var btnView2 = makeButtonView(model,'button2');
    var svgView = makeSVGView(model, 'svg');
    var controller = makeController(model);

    model.register(btnView.render);
    model.register(svgView.render);
    model.register(btnView2.render);
    btnView.register(controller.dispatch);
    btnView2.register(controller.dispatch);
});
